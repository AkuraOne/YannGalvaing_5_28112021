let fetch = require('node-fetch');

module.exports = {
    ProductCart: class ProductCart{
        constructor(keyOfsessionStorage,originalProduct,color,quantity){
            this.keyOfsessionStorage=keyOfsessionStorage;
            this.originalProduct=originalProduct;
            this.color=color;
            this.totalPrice=originalProduct.price*parseInt(quantity);
            this.quantity=parseInt(quantity);
        }
    },
    Contact: class Contact{
        constructor(firstName,lastName,address,city,email){
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.city = city;
            this.email = email;
        }
    },
    port: "3000",
    server: /*"http://akura.fr:3000",*/"http://localhost:3000",
    api: /*"http://akura.fr:3000/api/products/",*/"http://localhost:3000/api/products/",
    
    cart: {
        "107fb5b75607497b96722bda5b504926|Blue": 5,
        "415b7cacb65d43b2b5c1ff70f3393ad1|Black/Yellow": 20,
        "055743915a544fde83cfdfc904935ee7|Orange": 30
    },

    cart2: {
        "77711f0e466b4ddf953f677d30b0efc9|Grey": 1,
        "034707184e8e4eefb46400b5a3774b5f|Red": 4,
        "a6ec5b49bd164d7fbe10f37b6363f9fb|Brown": 6
    },
    
    /*products: [{colors:["Blue","White","Black"],_id:"107fb5b75607497b96722bda5b504926",name:"Kanap Sinopé",price:1849,imageUrl:"http://localhost:3000/images/kanap01.jpeg",description:"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",altTxt:"Photo d'un canapé bleu, deux places"},
    {colors:["Black/Yellow","Black/Red"],_id:"415b7cacb65d43b2b5c1ff70f3393ad1",name:"Kanap Cyllène",price:4499,imageUrl:"http://localhost:3000/images/kanap02.jpeg",description:"Morbi nec erat aliquam, sagittis urna non, laoreet justo. Etiam sit amet interdum diam, at accumsan lectus.",altTxt:"Photo d'un canapé jaune et noir, quattre places"},
    {colors:["Green","Red","Orange"],_id:"055743915a544fde83cfdfc904935ee7",name:"Kanap Calycé",price:3199,imageUrl:"http://localhost:3000/images/kanap03.jpeg",description:"Pellentesque fermentum arcu venenatis ex sagittis accumsan. Vivamus lacinia fermentum tortor.Mauris imperdiet tellus ante.",altTxt:"Photo d'un canapé d'angle, vert, trois places"},
    {colors:["Pink","White"],_id:"a557292fe5814ea2b15c6ef4bd73ed83",name:"Kanap Autonoé",price:1499,imageUrl:"http://localhost:3000/images/kanap04.jpeg",description:"Donec mattis nisl tortor, nec blandit sapien fermentum at. Proin hendrerit efficitur fringilla. Lorem ipsum dolor sit amet.",altTxt:"Photo d'un canapé rose, une à deux place"},
    {colors:["Grey","Purple","Blue"],_id:"8906dfda133f4c20a9d0e34f18adcf06",name:"Kanap Eurydomé",price:2249,imageUr:"http://localhost:3000/images/kanap05.jpeg",description:"Ut laoreet vulputate neque in commodo. Suspendisse maximus quis erat in sagittis. Donec hendrerit purus at congue aliquam.",altTxt:"Photo d'un canapé gris, trois places"},
    {colors:["Grey","Navy"],_id:"77711f0e466b4ddf953f677d30b0efc9",name:"Kanap Hélicé",price:999,imageUrl:"http://localhost:3000/images/kanap06.jpeg",description:"Curabitur vel augue sit amet arcu aliquet interdum. Integer vel quam mi. Morbi nec vehicula mi, sit amet vestibulum.",altTxt:"Photo d'un canapé gris, deux places"},
    {colors:["Red","Silver"],_id:"034707184e8e4eefb46400b5a3774b5f",name:"Kanap Thyoné",price:1999,imageUrl:"http://localhost:3000/images/kanap07.jpeg",description:"EMauris imperdiet tellus ante, sit amet pretium turpis molestie eu. Vestibulum et egestas eros. Vestibulum non lacus orci.",altTxt:"Photo d'un canapé rouge, deux places"},
    {colors:["Pink","Brown","Yellow","White"],_id:"a6ec5b49bd164d7fbe10f37b6363f9fb",name:"Kanap orthosie",price:3999,imageUrl:"http://localhost:3000/images/kanap08.jpeg",description:"Mauris molestie laoreet finibus. Aenean scelerisque convallis lacus at dapibus. Morbi imperdiet enim metus rhoncus.",altTxt:"Photo d'un canapé rose, trois places"}],   
    */

    getQueryAllProducts: async function(){
        return await fetch(this.api).then(datas=>datas.json());
    },

    getQueryProduct: async function(id){
        return await fetch(this.api + "" + id).then(data=>data.json());
    },

    getDiff: function(value1,value2){
        let intValue1 = parseInt(value1);
        let intValue2 = parseInt(value2);
        
        if(intValue1 != NaN && intValue2 != NaN){
            return -(intValue1 - intValue2);
        }

        throw new Error();
    },

    getTotalPriceOfCart: async function(cart){
        let totalPrice = 0;

        for(let key in cart){
            if(cart[key] != null){
                let productCart = await this.getProductCart(key, cart);
                totalPrice += productCart.totalPrice;
            }
        }

        return totalPrice;
    },
    getProductCart: async function(keyOfCart, cart){
        let id = keyOfCart.split('|')[0];
        let color = keyOfCart.split('|')[1];
        let quantity = cart[keyOfCart];
        let product = await this.getQueryProduct(id);
        
        return new this.ProductCart(keyOfCart,product,color,quantity);
    },
    getTotalQuantityOfProductsInCart: function(cart){
        let count = 0;
        for(let key in cart){
            if(cart[key] != null){
                count += parseInt(cart[key]);
            }
        }

        return count;
    },
    sendOrder: async function(contact,cart){
        let orderAddress = this.api + "/order";
        let idsOfProducts = [];
        let count = 0;

        for(let key in cart){
            if(count == cart.length){
                break;
            }

            let id = key.split('|')[0];
            if(idsOfProducts.find((value)=> value == id) == null && key != 'length'){
               idsOfProducts.push(key.split('|')[0]); 
            }

            count++;
        }

        let jsonDatas = JSON.stringify({contact: contact,products: idsOfProducts});

        const response = await fetch(orderAddress, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: jsonDatas,
        });

        return await response.json();  
    }
};

