/*the real datas in the api server 
[{colors:["Blue","White","Black"],_id:"107fb5b75607497b96722bda5b504926",name:"Kanap Sinopé",price:1849,imageUrl:"http://localhost:3000/images/kanap01.jpeg",description:"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",altTxt:"Photo d'un canapé bleu, deux places"},
{colors:["Black/Yellow","Black/Red"],_id:"415b7cacb65d43b2b5c1ff70f3393ad1",name:"Kanap Cyllène",price:4499,imageUrl:"http://localhost:3000/images/kanap02.jpeg",description:"Morbi nec erat aliquam, sagittis urna non, laoreet justo. Etiam sit amet interdum diam, at accumsan lectus.",altTxt:"Photo d'un canapé jaune et noir, quattre places"},
{colors:["Green","Red","Orange"],_id:"055743915a544fde83cfdfc904935ee7",name:"Kanap Calycé",price:3199,imageUrl:"http://localhost:3000/images/kanap03.jpeg",description:"Pellentesque fermentum arcu venenatis ex sagittis accumsan. Vivamus lacinia fermentum tortor.Mauris imperdiet tellus ante.",altTxt:"Photo d'un canapé d'angle, vert, trois places"},
{colors:["Pink","White"],_id:"a557292fe5814ea2b15c6ef4bd73ed83",name:"Kanap Autonoé",price:1499,imageUrl:"http://localhost:3000/images/kanap04.jpeg",description:"Donec mattis nisl tortor, nec blandit sapien fermentum at. Proin hendrerit efficitur fringilla. Lorem ipsum dolor sit amet.",altTxt:"Photo d'un canapé rose, une à deux place"},
{colors:["Grey","Purple","Blue"],_id:"8906dfda133f4c20a9d0e34f18adcf06",name:"Kanap Eurydomé",price:2249,imageUr:"http://localhost:3000/images/kanap05.jpeg",description:"Ut laoreet vulputate neque in commodo. Suspendisse maximus quis erat in sagittis. Donec hendrerit purus at congue aliquam.",altTxt:"Photo d'un canapé gris, trois places"},
{colors:["Grey","Navy"],_id:"77711f0e466b4ddf953f677d30b0efc9",name:"Kanap Hélicé",price:999,imageUrl:"http://localhost:3000/images/kanap06.jpeg",description:"Curabitur vel augue sit amet arcu aliquet interdum. Integer vel quam mi. Morbi nec vehicula mi, sit amet vestibulum.",altTxt:"Photo d'un canapé gris, deux places"},
{colors:["Red","Silver"],_id:"034707184e8e4eefb46400b5a3774b5f",name:"Kanap Thyoné",price:1999,imageUrl:"http://localhost:3000/images/kanap07.jpeg",description:"EMauris imperdiet tellus ante, sit amet pretium turpis molestie eu. Vestibulum et egestas eros. Vestibulum non lacus orci.",altTxt:"Photo d'un canapé rouge, deux places"},
{colors:["Pink","Brown","Yellow","White"],_id:"a6ec5b49bd164d7fbe10f37b6363f9fb",name:"Kanap orthosie",price:3999,imageUrl:"http://localhost:3000/images/kanap08.jpeg",description:"Mauris molestie laoreet finibus. Aenean scelerisque convallis lacus at dapibus. Morbi imperdiet enim metus rhoncus.",altTxt:"Photo d'un canapé rose, trois places"}]
*/
/*the mocked carts in the api of test
cart: {
    "107fb5b75607497b96722bda5b504926|Blue": 5,
    "415b7cacb65d43b2b5c1ff70f3393ad1|Black/Yellow": 20,
    "055743915a544fde83cfdfc904935ee7|Orange": 30
}
cart2: {
    "77711f0e466b4ddf953f677d30b0efc9|Grey": 1,
    "034707184e8e4eefb46400b5a3774b5f|Red": 4,
    "a6ec5b49bd164d7fbe10f37b6363f9fb|Brown": 6
}*/

//Prototype of order number which retrieve in object after have sent order => c555ebf0-5777-11ec-9793-916d37543721 => 459a60c0-5778-11ec-9793-916d37543721
   
 let expect = require('chai').expect;
//let sinon = require('sinon');
let queries = require('./lib/apiQueries.js');;

describe('Test of Queries', ()=>{
    it('Retrieve all products', async ()=>{
        let products = await queries.getQueryAllProducts();
        expect(products.length > 0).to.be.equal(true, "Erreur de communication avec le serveur http://localhost:3000/api/products/" + " => " + products);
    });

    
    it('Retrieve a product by id', async ()=>{
        let product = await queries.getQueryProduct('107fb5b75607497b96722bda5b504926');
        expect(product.price).to.be.equal(1849);
        expect(product._id).to.be.equal('107fb5b75607497b96722bda5b504926');
    });
    
    
    it('Send order with contact and cart and retrieve the order number', async ()=>{
        let superContact = new queries.Contact("Luc","Skywalker","1024 route de l'octet","Mos Espa","luc.skywalker@tatooine.com");
        let objWithOrderNumber = await queries.sendOrder(superContact,queries.cart);

        expect(objWithOrderNumber.orderId.length).to.be.equal(36,"Une erreur est survenue dans le test1. L'objet retourné => " + JSON.stringify(objWithOrderNumber));
    });

});

describe('Test of Presenter', ()=>{
    it('Give the difference between two relative numbers, the first being the initial starting number', ()=>{
        expect(queries.getDiff(85,95)).to.be.equal(+10);
        expect(queries.getDiff(85,55)).to.be.equal(-30);
        expect(queries.getDiff(85,0)).to.be.equal(-85);
        expect(queries.getDiff(85,85)).to.be.equal(0);
        expect(queries.getDiff("85","55")).to.be.equal(-30);
    });

    it('Give the total price in taking into account of each quantity of products in cart', async ()=>{
        expect(await queries.getTotalPriceOfCart(queries.cart)).to.be.equal(1849*5 + 4499*20 + 3199*30,"test 1 échoué");
        expect(await queries.getTotalPriceOfCart(queries.cart2)).to.be.equal(999*1 + 1999*4 + 3999*6, "test 2 échoué");
    });

    it('Reurn the total quantity of all products present in the cart',()=>{
        let totalQuantityOfCart1 = queries.getTotalQuantityOfProductsInCart(queries.cart);
        let totalQuantityOfCart2 = queries.getTotalQuantityOfProductsInCart(queries.cart2);

        expect(totalQuantityOfCart1).to.be.equal(55);
        expect(totalQuantityOfCart2).to.be.equal(11);

    });

    it('Create a ProductCart especially for use in cart because cart is a sessionStorage and it\'s limited to stock reals datas', async ()=>{
        let productCart = await queries.getProductCart('107fb5b75607497b96722bda5b504926|Blue',queries.cart);
        expect(productCart.quantity).to.be.equal(5,"Ereur test 1");

        let productCart2 = await queries.getProductCart('034707184e8e4eefb46400b5a3774b5f|Red',queries.cart2);
        expect(productCart2.quantity).to.be.equal(4,"Erreur test2");
    });

});