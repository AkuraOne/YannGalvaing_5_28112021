"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _cartPresenter = _interopRequireDefault(require("../utils/cartPresenter.js"));

var cartPresenter = new _cartPresenter["default"]();
cartPresenter.fillCart();
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _confirmationPresenter = _interopRequireDefault(require("../utils/confirmationPresenter.js"));

var confirmationPresenter = new _confirmationPresenter["default"]();
confirmationPresenter.fillOrderId();
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _indexPresenter = _interopRequireDefault(require("../utils/indexPresenter.js"));

var indexPresenter = new _indexPresenter["default"]();
indexPresenter.fillProducts();
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _productPresenter = _interopRequireDefault(require("../utils/productPresenter.js"));

var productPresenter = new _productPresenter["default"]();
productPresenter.fillProduct();
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _presenter = _interopRequireDefault(require("./core/presenter.js"));

var _loadingSpinner = _interopRequireDefault(require("./loading-spinner.js"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var CartPresenter = /*#__PURE__*/function (_Presenter) {
  (0, _inherits2["default"])(CartPresenter, _Presenter);

  var _super = _createSuper(CartPresenter);

  function CartPresenter() {
    var _this;

    (0, _classCallCheck2["default"])(this, CartPresenter);
    _this = _super.call(this);
    _this.productsCart = [];
    _this.firstNameIsOk = false;
    _this.lastNameIsOk = false;
    _this.addressIsOk = false;
    _this.cityIsOk = false;
    _this.emailIsOk = false;
    _this.cartItems = document.querySelector('#cart__items');
    _this.spinner = new _loadingSpinner["default"](_this.cartItems);
    return _this;
  } //function which fill the cart.html page


  (0, _createClass2["default"])(CartPresenter, [{
    key: "fillCart",
    value: function () {
      var _fillCart = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
        var _this2 = this;

        var orderButton, _orderButton, nbOfProducts, count, _loop, key, _ret;

        return _regenerator["default"].wrap(function _callee2$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                if (!(this.cart.length == 0)) {
                  _context3.next = 7;
                  break;
                }

                alert("Votre panier est vide");
                orderButton = document.querySelector('#order');
                orderButton.disabled = true;
                return _context3.abrupt("return");

              case 7:
                _orderButton = document.querySelector('#order');
                _orderButton.disabled = false;

              case 9:
                this.spinner.show(); //creation of the products which have added to cart

                nbOfProducts = this.cart.length;

                if (!(nbOfProducts == 0)) {
                  _context3.next = 13;
                  break;
                }

                return _context3.abrupt("return");

              case 13:
                count = 0;
                _loop = /*#__PURE__*/_regenerator["default"].mark(function _loop(key) {
                  var productCart, totalQuantity, totalPrice, aProductPage, article, div, img, div2, div3, h2, p, pPrice, div4, div5, pQte, inputQuantity, div6, a, iconeDelete, _orderButton2;

                  return _regenerator["default"].wrap(function _loop$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          _context2.next = 2;
                          return _this2.getProductCart(key);

                        case 2:
                          productCart = _context2.sent;

                          _this2.productsCart.push(productCart); //fill the DOM elements with event listener for update the cart


                          totalQuantity = document.querySelector('#totalQuantity');
                          totalPrice = document.querySelector('#totalPrice');
                          aProductPage = document.createElement('a');
                          aProductPage.setAttribute('href', './product.html?id=' + productCart.originalProduct._id);
                          article = document.createElement('article');
                          article.classList.add("cart__item");
                          article.setAttribute('id', productCart.originalProduct._id);
                          article.setAttribute('data-color', productCart.color);
                          div = document.createElement('div');
                          div.classList.add("cart__item__img");
                          article.appendChild(div);
                          img = document.createElement('img');
                          img.setAttribute('src', productCart.originalProduct.imageUrl);
                          img.setAttribute('alt', productCart.originalProduct.altTxt);
                          aProductPage.appendChild(img);
                          div.appendChild(aProductPage);
                          div2 = document.createElement('div');
                          div2.classList.add("cart__item__content");
                          article.appendChild(div2);
                          div3 = document.createElement('div');
                          div3.classList.add("cart__item__content__description");
                          article.appendChild(div3);
                          h2 = document.createElement('h2');
                          h2.textContent = productCart.originalProduct.name;
                          div3.appendChild(h2);
                          p = document.createElement('p');
                          p.textContent = productCart.color;
                          div3.appendChild(p);
                          pPrice = document.createElement('p');
                          pPrice.textContent = productCart.originalProduct.price + " €";
                          div3.appendChild(pPrice);
                          div4 = document.createElement('div');
                          div4.classList.add("cart__item__content__settings");
                          article.appendChild(div4);
                          div5 = document.createElement('div');
                          div5.classList.add("cart__item__content__settings__quantity");
                          div4.appendChild(div5);
                          pQte = document.createElement('p');
                          pQte.textContent = "Qte : ";
                          div5.appendChild(pQte);
                          inputQuantity = document.createElement('input');
                          inputQuantity.setAttribute('type', 'number');
                          inputQuantity.setAttribute('name', 'itemQuantity');
                          inputQuantity.setAttribute('min', '1');
                          inputQuantity.setAttribute('max', '100');
                          inputQuantity.classList.add('itemQuantity');
                          inputQuantity.value = productCart.quantity;
                          inputQuantity.addEventListener('input', function (e) {
                            e.preventDefault();

                            _this2.setQuantityOfProductsInCart(productCart, e.target.value);

                            _this2.setTotalPriceOfProductsInCart(productCart);
                          });
                          div5.appendChild(inputQuantity);
                          div6 = document.createElement('div');
                          div6.classList.add("cart__item__content__settings__delete");
                          article.appendChild(div6);
                          a = document.createElement('a'); //a.textContent = "Supprimer";
                          //a.setAttribute("data-idStorage",productCart.originalProduct._id + "|" + productCart.color);

                          a.setAttribute("href", "#");
                          a.classList.add("deleteItem");
                          a.addEventListener('click', function (e) {
                            e.preventDefault();

                            _this2.deleteProductOfCart(e.target);
                          });
                          iconeDelete = document.createElement('img');
                          iconeDelete.setAttribute('src', '../../images/icons/trash.svg');
                          iconeDelete.setAttribute('alt', 'Supprimer la ligne de produit du panier');
                          iconeDelete.setAttribute("data-idStorage", productCart.originalProduct._id + "|" + productCart.color);
                          iconeDelete.style.height = '40px';
                          iconeDelete.style.width = '80px';
                          iconeDelete.style.textAlign = 'center';
                          a.appendChild(iconeDelete);
                          div6.appendChild(a);
                          document.querySelector('#cart__items').appendChild(article); //if we are to the end of products list we update displayed infos in page bottom

                          count++;

                          if (!(count == nbOfProducts)) {
                            _context2.next = 81;
                            break;
                          }

                          totalQuantity.textContent = _this2.getTotalQuantityOfProductsInCart();
                          _context2.next = 75;
                          return _this2.getTotalPriceOfProductCart();

                        case 75:
                          totalPrice.textContent = _context2.sent;
                          setTimeout(function () {
                            _this2.spinner.hide();
                          }, 1000); //initialisation of formulary the sent datas

                          _this2.initControlForm(); //initialisation of button of datas to send


                          _orderButton2 = document.querySelector('#order');

                          _orderButton2.addEventListener('click', /*#__PURE__*/function () {
                            var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(e) {
                              var firstNameInput, lastNameInput, addressInput, cityInput, emailInput, contact, orderNumber;
                              return _regenerator["default"].wrap(function _callee$(_context) {
                                while (1) {
                                  switch (_context.prev = _context.next) {
                                    case 0:
                                      e.preventDefault();

                                      if (!_this2.isFormOk()) {
                                        _context.next = 14;
                                        break;
                                      }

                                      firstNameInput = document.querySelector('#firstName');
                                      lastNameInput = document.querySelector('#lastName');
                                      addressInput = document.querySelector('#address');
                                      cityInput = document.querySelector('#city');
                                      emailInput = document.querySelector('#email');
                                      contact = _this2.getNewContact(firstNameInput.value, lastNameInput.value, addressInput.value, cityInput.value, emailInput.value);
                                      _context.next = 10;
                                      return _this2.sendOrderToAPI(contact);

                                    case 10:
                                      orderNumber = _context.sent;

                                      if (orderNumber != null || orderNumber != undefined) {
                                        window.location = '../../html/confirmation.html?orderNumber=' + orderNumber.orderId;
                                      } else {
                                        alert("Le serveur est temporairement inaccessible. Veuillez réessayer dans un instant.");
                                      }

                                      _context.next = 15;
                                      break;

                                    case 14:
                                      alert("Votre formulaire de contact n'est pas complet ou des champs sont incorrects");

                                    case 15:
                                    case "end":
                                      return _context.stop();
                                  }
                                }
                              }, _callee);
                            }));

                            return function (_x) {
                              return _ref.apply(this, arguments);
                            };
                          }());

                          return _context2.abrupt("return", {
                            v: void 0
                          });

                        case 81:
                        case "end":
                          return _context2.stop();
                      }
                    }
                  }, _loop);
                });
                _context3.t0 = _regenerator["default"].keys(this.cart);

              case 16:
                if ((_context3.t1 = _context3.t0()).done) {
                  _context3.next = 24;
                  break;
                }

                key = _context3.t1.value;
                return _context3.delegateYield(_loop(key), "t2", 19);

              case 19:
                _ret = _context3.t2;

                if (!((0, _typeof2["default"])(_ret) === "object")) {
                  _context3.next = 22;
                  break;
                }

                return _context3.abrupt("return", _ret.v);

              case 22:
                _context3.next = 16;
                break;

              case 24:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee2, this);
      }));

      function fillCart() {
        return _fillCart.apply(this, arguments);
      }

      return fillCart;
    }() //remove the product in cart

  }, {
    key: "deleteProductOfCart",
    value: function deleteProductOfCart(target) {
      try {
        //retrieve id and color from target (a)
        var idOfThisCart = target.getAttribute('data-idStorage'); //retrieve productCart

        var productCart = this.productsCart.find(function (pdtCart) {
          return pdtCart.keyInThisCart == idOfThisCart;
        }); //retrieve article DOM

        var articleToDelete = document.querySelector('article[id="' + productCart.originalProduct._id + '"][data-color=\"' + productCart.color + '\"]'); //update datas (quantity and total price)

        this.setQuantityOfProductsInCart(productCart, 0);
        this.setTotalPriceOfProductsInCart(productCart, productCart.totalPrice); //delete productCart from cartPresenter array

        var indexOfProduct = this.productsCart.indexOf(productCart);
        this.productsCart.splice(indexOfProduct, indexOfProduct); //remove temporary productCart from this.cart array AT THE END

        this.cart.removeItem(productCart.keyInThisCart);
        document.querySelector('#cart__items').removeChild(articleToDelete);
        alert('Votre ' + productCart.originalProduct.name + ' ' + productCart.color + " a été supprimé du panier !");
      } catch (err) {
        console.log('Erreur dans la fonction de suppression : ' + err);
      }
    } //upgrade the quantity of product in cart

  }, {
    key: "setQuantityOfProductsInCart",
    value: function setQuantityOfProductsInCart(productCart, newValue) {
      var totalQuantity = document.getElementById('totalQuantity');
      var diffQuantity = this.getDiff(productCart.quantity, newValue);
      productCart.quantity = parseInt(productCart.quantity) + diffQuantity;
      this.cart.setItem(productCart.keyInThisCart, productCart.quantity);
      totalQuantity.textContent = parseInt(totalQuantity.textContent) + diffQuantity;
    } //upgrade the quantity of product in cart

  }, {
    key: "setTotalPriceOfProductsInCart",
    value: function setTotalPriceOfProductsInCart(productCart) {
      var totalPrice = document.getElementById('totalPrice');
      var newTotalPrice = productCart.originalProduct.price * productCart.quantity;
      var diffPrice = this.getDiff(productCart.totalPrice, newTotalPrice);
      productCart.totalPrice = newTotalPrice;
      totalPrice.textContent = parseInt(totalPrice.textContent) + diffPrice;
    } //controls for form before send to order API

  }, {
    key: "initControlForm",
    value: function initControlForm() {
      var _this3 = this;

      //control firstName
      var firstNameInput = document.querySelector('#firstName');
      firstNameInput.addEventListener('input', function (e) {
        var firstNameMesgError = document.querySelector('#firstNameErrorMsg');
        var regexp = /^([a-zA-Z]){2,}([-|\s]{0,1}[a-zA-Z]{2,}){0,1}$/;
        var errorMessage = "* Le prénom doit contenir des lettres, seul les caractère (-, un espace) sont acceptés pour les prénoms composés";
        firstNameMesgError.textContent = _this3.controlExpression(e.target.value, regexp, errorMessage);
        _this3.firstNameIsOk = firstNameMesgError.textContent == "";
      }); //control lastName

      var lastNameInput = document.querySelector('#lastName');
      lastNameInput.addEventListener('input', function (e) {
        var lastNameMesgError = document.querySelector('#lastNameErrorMsg');
        var regexp = /^([a-zA-Z]){2,}([-|\s]{0,1}[a-zA-Z]{2,}){0,1}$/;
        var errorMessage = "* Le nom doit contenir des lettres, seul les caractère (-, un espace) sont acceptés pour les noms composés";
        lastNameMesgError.textContent = _this3.controlExpression(e.target.value, regexp, errorMessage);
        _this3.lastNameIsOk = lastNameMesgError.textContent == "";
      }); //control address

      var addressInput = document.querySelector('#address');
      addressInput.addEventListener('input', function (e) {
        var addressMesgError = document.querySelector('#addressErrorMsg');
        var regexp = /^([a-zA-Z]{2,}|[0-9]{1,})(\s{0,1}([a-zA-Z]-{0,1}){2,}){1,}/;
        var errorMessage = "* L'adresse doit commencer par un numéro de voie ou la voie directe s'il n'y a pas de numéro";
        addressMesgError.textContent = _this3.controlExpression(e.target.value, regexp, errorMessage);
        _this3.addressIsOk = addressMesgError.textContent == "";
      }); //control city

      var cityInput = document.querySelector('#city');
      cityInput.addEventListener('input', function (e) {
        var cityMesgError = document.querySelector('#cityErrorMsg');
        var regexp = /^([a-zA-Z]){2,}([-|\s]{0,1}[a-zA-Z]{2,}){0,}$/;
        var errorMessage = "* Le nom de ville doit contenir des lettres, seul les caractère (-, un espace) sont acceptés pour les noms de ville composés";
        cityMesgError.textContent = _this3.controlExpression(e.target.value, regexp, errorMessage);
        _this3.cityIsOk = cityMesgError.textContent == "";
      }); //control email

      var emailInput = document.querySelector('#email');
      emailInput.addEventListener('input', function (e) {
        var emailMesgError = document.querySelector('#emailErrorMsg');
        var regexp = /^([a-z0-9])([-|_|\.]{0,1}[a-z0-9]){0,}(@{1}[a-z]{2,}\.{1}[a-z]{2,})$/;
        var errorMessage = "* L'adresse email doit être de se format adresse.quelquechose@chose.fr, adresse47.quelques10choses@chose.fr";
        emailMesgError.textContent = _this3.controlExpression(e.target.value, regexp, errorMessage);
        _this3.emailIsOk = emailMesgError.textContent == "";
      });
    }
  }, {
    key: "isFormOk",
    value: function isFormOk() {
      var formIsOk = this.firstNameIsOk && this.lastNameIsOk && this.addressIsOk && this.cityIsOk && this.emailIsOk;
      return formIsOk;
    }
  }]);
  return CartPresenter;
}(_presenter["default"]);

exports["default"] = CartPresenter;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _presenter = _interopRequireDefault(require("./core/presenter.js"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var ConfirmationPresenter = /*#__PURE__*/function (_Presenter) {
  (0, _inherits2["default"])(ConfirmationPresenter, _Presenter);

  var _super = _createSuper(ConfirmationPresenter);

  function ConfirmationPresenter() {
    (0, _classCallCheck2["default"])(this, ConfirmationPresenter);
    return _super.call(this);
  }

  (0, _createClass2["default"])(ConfirmationPresenter, [{
    key: "fillOrderId",
    value: function fillOrderId() {
      var orderID = new URL(window.location.toString()).searchParams.get('orderNumber');

      if (orderID != undefined && orderID != null) {
        this.deleteCart();
      }

      document.querySelector('#orderId').textContent = orderID;
    }
  }, {
    key: "deleteCart",
    value: function deleteCart() {
      sessionStorage.clear();
    }
  }]);
  return ConfirmationPresenter;
}(_presenter["default"]);

exports["default"] = ConfirmationPresenter;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.ProductCart = exports.Contact = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _queries = _interopRequireDefault(require("./queries.js"));

var Presenter = /*#__PURE__*/function () {
  function Presenter() {
    var queries = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    (0, _classCallCheck2["default"])(this, Presenter);
    this.queries = queries !== null && queries !== void 0 ? queries : new _queries["default"](); //=> La vraie instruction binaire qui est remplacée par la ternaire du dessous

    this.cart = sessionStorage; //this.queries = (queries == null) ? new Queries() : queries;
    //this.queries = new Queries();
  } //get all products


  (0, _createClass2["default"])(Presenter, [{
    key: "getQueryAllProducts",
    value: function () {
      var _getQueryAllProducts = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.queries.getProducts();

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getQueryAllProducts() {
        return _getQueryAllProducts.apply(this, arguments);
      }

      return getQueryAllProducts;
    }() //get Id Of current product on product.html page

  }, {
    key: "getIdProduct",
    value: function getIdProduct() {
      try {
        /*let tab = window.location.toString().split("?id=");
        return tab[tab.length-1];*/
        return new URL(window.location.toString()).searchParams.get('id');
      } catch (err) {
        console.log("Erreur : " + err);
      }
    } //get the api product

  }, {
    key: "getQueryProduct",
    value: function () {
      var _getQueryProduct = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.queries.getProduct(this.getIdProduct());

              case 2:
                return _context2.abrupt("return", _context2.sent);

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function getQueryProduct() {
        return _getQueryProduct.apply(this, arguments);
      }

      return getQueryProduct;
    }() //get product for the cart.html page

  }, {
    key: "getProductCart",
    value: function () {
      var _getProductCart = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(keyInThisCart) {
        var id, color, quantity, originalProduct;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                id = keyInThisCart.split('|')[0];
                color = keyInThisCart.split('|')[1];
                quantity = this.cart.getItem(keyInThisCart);
                _context3.next = 5;
                return this.queries.getProduct(id);

              case 5:
                originalProduct = _context3.sent;
                return _context3.abrupt("return", new ProductCart(keyInThisCart, originalProduct, color, quantity));

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function getProductCart(_x) {
        return _getProductCart.apply(this, arguments);
      }

      return getProductCart;
    }() //get the total quantity of product present in cart

  }, {
    key: "getTotalQuantityOfProductsInCart",
    value: function getTotalQuantityOfProductsInCart() {
      var count = 0;

      for (var key in this.cart) {
        if (this.cart.getItem(key) != null) {
          count += parseInt(this.cart.getItem(key));
        }
      }

      return count;
    } //get the total price of one product in cart

  }, {
    key: "getTotalPriceOfProductCart",
    value: function () {
      var _getTotalPriceOfProductCart = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4() {
        var totalPrice, key, productCart;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                totalPrice = 0;
                _context4.t0 = _regenerator["default"].keys(this.cart);

              case 2:
                if ((_context4.t1 = _context4.t0()).done) {
                  _context4.next = 11;
                  break;
                }

                key = _context4.t1.value;

                if (!(this.cart.getItem(key) != null)) {
                  _context4.next = 9;
                  break;
                }

                _context4.next = 7;
                return this.getProductCart(key);

              case 7:
                productCart = _context4.sent;
                totalPrice += productCart.totalPrice;

              case 9:
                _context4.next = 2;
                break;

              case 11:
                return _context4.abrupt("return", totalPrice);

              case 12:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function getTotalPriceOfProductCart() {
        return _getTotalPriceOfProductCart.apply(this, arguments);
      }

      return getTotalPriceOfProductCart;
    }() //get the difference of two values

  }, {
    key: "getDiff",
    value: function getDiff(value1, value2) {
      return -(parseInt(value1) - parseInt(value2));
    } //return boolean if value respect the pattern

  }, {
    key: "IsOk",
    value: function IsOk(value, regex) {
      return value.match(regex) != null;
    } //control and return error message if pattern don't match with value

  }, {
    key: "controlExpression",
    value: function controlExpression(value, regexp, errorMessage) {
      if (this.IsOk(value, regexp)) {
        return "";
      } else {
        return errorMessage;
      }
    } //sent order to API

  }, {
    key: "sendOrderToAPI",
    value: function () {
      var _sendOrderToAPI = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(contact) {
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return this.queries.sendOrder(contact);

              case 2:
                return _context5.abrupt("return", _context5.sent);

              case 3:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function sendOrderToAPI(_x2) {
        return _sendOrderToAPI.apply(this, arguments);
      }

      return sendOrderToAPI;
    }() //return un new Contact instance

  }, {
    key: "getNewContact",
    value: function getNewContact(firstName, lastName, address, city, email) {
      return new Contact(firstName, lastName, address, city, email);
    }
  }]);
  return Presenter;
}();

exports["default"] = Presenter;

var ProductCart = function ProductCart(keyInThisCart, originalProduct, color, quantity) {
  (0, _classCallCheck2["default"])(this, ProductCart);
  this.keyInThisCart = keyInThisCart;
  this.originalProduct = originalProduct;
  this.color = color;
  this.totalPrice = originalProduct.price * quantity;
  this.quantity = parseInt(quantity);
};

exports.ProductCart = ProductCart;

var Contact = function Contact(firstName, lastName, address, city, email) {
  (0, _classCallCheck2["default"])(this, Contact);
  this.firstName = firstName;
  this.lastName = lastName;
  this.address = address;
  this.city = city;
  this.email = email;
};

exports.Contact = Contact;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var Queries = /*#__PURE__*/function () {
  function Queries() {
    var _this = this;

    (0, _classCallCheck2["default"])(this, Queries);
    (0, _defineProperty2["default"])(this, "getProducts", /*#__PURE__*/(0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return fetch(_this.api).then(function (datas) {
                return datas.json();
              });

            case 3:
              return _context.abrupt("return", _context.sent);

            case 6:
              _context.prev = 6;
              _context.t0 = _context["catch"](0);
              console.log("Erreur : " + _context.t0);

            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 6]]);
    })));
    (0, _defineProperty2["default"])(this, "getProduct", /*#__PURE__*/function () {
      var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(id) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return fetch(_this.api + "" + id).then(function (data) {
                  return data.json();
                });

              case 3:
                return _context2.abrupt("return", _context2.sent);

              case 6:
                _context2.prev = 6;
                _context2.t0 = _context2["catch"](0);
                console.log("Erreur : " + _context2.t0);

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 6]]);
      }));

      return function (_x) {
        return _ref2.apply(this, arguments);
      };
    }());
    (0, _defineProperty2["default"])(this, "sendOrder", /*#__PURE__*/function () {
      var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(contact) {
        var orderAddress, idsOfProducts, count, _loop, key, _ret, jsonDatas;

        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                orderAddress = _this.api + "/order";
                idsOfProducts = [];
                count = 0;

                _loop = function _loop(key) {
                  if (count == sessionStorage.length) {
                    return "break";
                  }

                  var id = key.split('|')[0];

                  if (idsOfProducts.find(function (value) {
                    return value == id;
                  }) == null && key != 'length') {
                    idsOfProducts.push(key.split('|')[0]);
                  }

                  count++;
                };

                _context3.t0 = _regenerator["default"].keys(sessionStorage);

              case 5:
                if ((_context3.t1 = _context3.t0()).done) {
                  _context3.next = 12;
                  break;
                }

                key = _context3.t1.value;
                _ret = _loop(key);

                if (!(_ret === "break")) {
                  _context3.next = 10;
                  break;
                }

                return _context3.abrupt("break", 12);

              case 10:
                _context3.next = 5;
                break;

              case 12:
                jsonDatas = JSON.stringify({
                  contact: contact,
                  products: idsOfProducts
                });
                _context3.next = 15;
                return _this.post(jsonDatas, orderAddress);

              case 15:
                return _context3.abrupt("return", _context3.sent);

              case 16:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      return function (_x2) {
        return _ref3.apply(this, arguments);
      };
    }());
    this.port = "3000";
    this.server = "http://" +
    /*"akura.fr"*/
    window.location.hostname + ":" + this.port;
    this.api = this.server + "/api/products/";
  } //post method to send datas


  (0, _createClass2["default"])(Queries, [{
    key: "post",
    value: function () {
      var _post = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(jsonDatas, url) {
        var response;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                  },
                  body: jsonDatas
                });

              case 3:
                response = _context4.sent;
                _context4.next = 6;
                return response.json();

              case 6:
                return _context4.abrupt("return", _context4.sent);

              case 9:
                _context4.prev = 9;
                _context4.t0 = _context4["catch"](0);
                alert("Une erreur de communication avec le serveur est survenue => " + _context4.t0);

              case 12:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 9]]);
      }));

      function post(_x3, _x4) {
        return _post.apply(this, arguments);
      }

      return post;
    }() //Get all api products

  }]);
  return Queries;
}();

exports["default"] = Queries;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _presenter = _interopRequireDefault(require("./core/presenter.js"));

var _loadingSpinner = _interopRequireDefault(require("./loading-spinner.js"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var indexPresenter = /*#__PURE__*/function (_Presenter) {
  (0, _inherits2["default"])(indexPresenter, _Presenter);

  var _super = _createSuper(indexPresenter);

  function indexPresenter() {
    var _this;

    (0, _classCallCheck2["default"])(this, indexPresenter);
    _this = _super.call(this);
    _this.items = document.getElementById('items');
    _this.spinner = new _loadingSpinner["default"](_this.items);
    return _this;
  } //function which fill the DOM elements with all products in the index.html page


  (0, _createClass2["default"])(indexPresenter, [{
    key: "fillProducts",
    value: function () {
      var _fillProducts = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
        var _this2 = this;

        var products;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.spinner.show(); //query for get all products

                _context.next = 3;
                return this.getQueryAllProducts();

              case 3:
                products = _context.sent;
                //fill the DOM elements index Page
                products.forEach(function (product) {
                  var a = document.createElement('a');
                  a.setAttribute('href', "./product.html?id=" + product._id);
                  var article = document.createElement('article');
                  var image = document.createElement('img');
                  image.setAttribute('src', product.imageUrl);
                  image.setAttribute('alt', product.altTxt);
                  var h3 = document.createElement('h3');
                  h3.classList.add("productName");
                  h3.textContent = product.name;
                  var p = document.createElement('p');
                  p.classList.add("productDescription");
                  p.textContent = product.description;
                  article.appendChild(image);
                  article.appendChild(h3);
                  article.appendChild(p);
                  a.appendChild(article);

                  _this2.items.appendChild(a);
                });
                setTimeout(function () {
                  _this2.spinner.hide();
                }, 1000);

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function fillProducts() {
        return _fillProducts.apply(this, arguments);
      }

      return fillProducts;
    }()
  }]);
  return indexPresenter;
}(_presenter["default"]);

exports["default"] = indexPresenter;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var Spinner = /*#__PURE__*/function () {
  function Spinner(parent) {
    (0, _classCallCheck2["default"])(this, Spinner);
    this.div = null;
    this.parent = parent;
    this.createSpinner();
  }

  (0, _createClass2["default"])(Spinner, [{
    key: "createSpinner",
    value: function createSpinner() {
      this.parent.style.cssText = 'position:relative;';
      this.div = document.createElement('div');
      var styles = "@-webkit-keyframes spin {" + "0% { -webkit-transform: rotate(0deg); }" + "100% { -webkit-transform: rotate(360deg); }" + "}" + "@keyframes spin {" + "0% { transform: rotate(0deg); }" + "100% { transform: rotate(360deg); }" + "}";
      var styleSheet = document.createElement("style");
      styleSheet.innerText = styles;
      document.head.appendChild(styleSheet);
      this.div.setAttribute('id', 'loading-spinner');
      this.div.style.cssText = "position: absolute;" + "top:45%;" + "left:45%;" + "background: url(../../images/logo.png);" + "background-size: cover;" + "box-shadow: 0px 0px 6px black;" + "border: 16px solid #f3f3f3;" + "border-radius: 50%;" + "border-top: 16px solid #3498db;" + "border-bottom: 16px solid #3d4c68;" + "width: 120px;" + "height: 120px;" + "-webkit-animation: spin 2s linear infinite;" + "animation: spin 2s linear infinite;";
      this.hide();
      this.parent.appendChild(this.div);
    }
  }, {
    key: "show",
    value: function show() {
      this.div.style.display = 'flex';
    }
  }, {
    key: "hide",
    value: function hide() {
      this.div.style.display = 'none';
    }
  }]);
  return Spinner;
}();

exports["default"] = Spinner;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _presenter = _interopRequireDefault(require("./core/presenter.js"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var ProductPresenter = /*#__PURE__*/function (_Presenter) {
  (0, _inherits2["default"])(ProductPresenter, _Presenter);

  var _super = _createSuper(ProductPresenter);

  function ProductPresenter() {
    (0, _classCallCheck2["default"])(this, ProductPresenter);
    return _super.call(this);
  } //function which fill the DOM elements with a product in the product.html page


  (0, _createClass2["default"])(ProductPresenter, [{
    key: "fillProduct",
    value: function () {
      var _fillProduct = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
        var _this = this;

        var product, img;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.getQueryProduct();

              case 2:
                product = _context.sent;
                //fill DOM elements
                document.title = product.name;
                img = document.createElement('img');
                img.setAttribute('src', product.imageUrl);
                img.setAttribute('alt', product.altTxt);
                document.querySelector('div.item__img').appendChild(img);
                document.querySelector('#title').textContent = product.name;
                document.querySelector('#price').textContent = product.price;
                document.querySelector('#description').textContent = product.description;
                product.colors.forEach(function (color) {
                  var option = document.createElement('option');
                  option.setAttribute('value', color);
                  option.textContent = color;
                  document.querySelector('#colors').appendChild(option);
                });
                document.querySelector('#addToCart').addEventListener('click', function (e) {
                  _this.addToCart();
                });

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function fillProduct() {
        return _fillProduct.apply(this, arguments);
      }

      return fillProduct;
    }() //add product to cart from the product.html page

  }, {
    key: "addToCart",
    value: function addToCart() {
      var quantityOfProduct = document.querySelector('#quantity').value;
      var selectedColor = document.querySelector('#colors').value; //check if color is selected

      if (selectedColor === "") {
        alert("Veuillez sélectionner une couleur s'il vous plait");
        return;
      }

      var key = this.getIdProduct() + "|" + selectedColor; //check if item exist

      if (this.cart.getItem(key)) {
        var quantity = parseInt(this.cart.getItem(key)) + parseInt(quantityOfProduct);
        this.cart.setItem(key, quantity);
      } else {
        this.cart.setItem(key, quantityOfProduct);
      }

      alert("Vous avez ajouté " + quantityOfProduct + " " + document.querySelector('#title').textContent + " dans votre panier");
    }
  }]);
  return ProductPresenter;
}(_presenter["default"]);

exports["default"] = ProductPresenter;
