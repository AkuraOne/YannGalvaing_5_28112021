export default class Queries{

    constructor(){
        this.port = "3000";
        this.server = "http://" + /*"akura.fr"*/window.location.hostname + ":" + this.port;
        this.api = this.server + "/api/products/";
    }

    //post method to send datas
    async post(jsonDatas,url){
        try{
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: jsonDatas,
            });

            return await response.json();  
        }
        catch(err){
            alert("Une erreur de communication avec le serveur est survenue => " + err);
        }
    }

    //Get all api products
    getProducts = async () => {
        try{
            return await fetch(this.api).then(datas=>datas.json());
        }
        catch(err){
            console.log("Erreur : " + err);
        }
    }

    //Get api product
    getProduct = async (id) => {
        try{
            return await fetch(this.api + "" + id).then(data=>data.json());
        }
        catch(err){
            console.log("Erreur : " + err);
        }
    }

    //Send new order to api
    sendOrder = async (contact) => {
        let orderAddress = this.api + "/order";
        let idsOfProducts = [];
        let count = 0;

        for(let key in localStorage){
            if(count == localStorage.length){
                break;
            }

            let id = key.split('|')[0];
            if(idsOfProducts.find((value)=> value == id) == null && key != 'length' && key != 'headingsMap_selectedTab'){
               idsOfProducts.push(key.split('|')[0]); 
            }

            count++;
        }

        let jsonDatas = JSON.stringify({contact: contact,products: idsOfProducts});
        return await this.post(jsonDatas,orderAddress);
    };
}

