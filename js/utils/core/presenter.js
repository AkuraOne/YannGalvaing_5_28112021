import Queries from "./queries.js";

export default class Presenter{

    constructor(queries=null){
        this.queries = queries?? new Queries();
        this.cart = localStorage;
    }

    //get all products
    async getQueryAllProducts(){
        return await this.queries.getProducts();
    }

    //get Id Of current product on product.html page
    getIdProduct(){
        try{
            /*let tab = window.location.toString().split("?id=");
            return tab[tab.length-1];*/
            return new URL(window.location.toString()).searchParams.get('id');
        }
        catch(err){
            console.log("Erreur : " + err);
        }
    }

    //get the api product
    async getQueryProduct(){
        //query for get product
        return await this.queries.getProduct(this.getIdProduct());
    }

    //get product for the cart.html page
    async getProductCart(keyInThisCart){
        let id = keyInThisCart.split('|')[0];
        let color = keyInThisCart.split('|')[1];
        let quantity = this.cart.getItem(keyInThisCart);
        let originalProduct = await this.queries.getProduct(id);
        
        return new ProductCart(keyInThisCart,originalProduct,color,quantity);
    }

    //get the total quantity of product present in cart
    getTotalQuantityOfProductsInCart(){
        let count = 0;
        for(let key in this.cart){
            if(this.cart.getItem(key) != null){
                count += parseInt(this.cart.getItem(key));
            }
        }

        return count;
    }

    //get the total price of one product in cart
    async getTotalPriceOfProductCart(){
        let totalPrice = 0;

        for(let key in this.cart){
            if(this.cart.getItem(key) != null){
                let productCart = await this.getProductCart(key);
                totalPrice += productCart.totalPrice;
            }
        }

        return totalPrice;
    }

    //get the difference of two values
    getDiff(value1,value2){
        return -(parseInt(value1) - parseInt(value2));
    }

    //return boolean if value respect the pattern
    IsOk(value,regex){
        return(value.match(regex) != null);
    }

    //control and return error message if pattern don't match with value
    controlExpression(value,regexp,errorMessage){
        if(this.IsOk(value,regexp)){
            return "";
        }else{
            return errorMessage;
        }
    }

    //sent order to API
    async sendOrderToAPI(contact){
        return await this.queries.sendOrder(contact);
    }

    //return un new Contact instance
    getNewContact(firstName,lastName,address,city,email){
        return new Contact(firstName,lastName,address,city,email);
    }
}

export class ProductCart{
    constructor(keyInThisCart,originalProduct,color,quantity){
        this.keyInThisCart=keyInThisCart;
        this.originalProduct=originalProduct;
        this.color=color;
        this.totalPrice=originalProduct.price*quantity;
        this.quantity=parseInt(quantity);
    }
}

export class Contact{
    constructor(firstName,lastName,address,city,email){
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.email = email;
    }
}