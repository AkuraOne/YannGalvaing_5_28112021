export default class Spinner{
	constructor(parent){
		this.div=null;
		this.parent = parent;
		this.createSpinner();
	}
	
	createSpinner(){
		this.parent.style.cssText = 'position:relative;';
		this.div = document.createElement('div');

		let styles = 
		"@-webkit-keyframes spin {" +
			"0% { -webkit-transform: rotate(0deg); }" +
			"100% { -webkit-transform: rotate(360deg); }" +
		"}" +
		"@keyframes spin {" +
			"0% { transform: rotate(0deg); }" +
			"100% { transform: rotate(360deg); }" +
		"}";

		let styleSheet = document.createElement("style");
		styleSheet.innerText = styles;
		document.head.appendChild(styleSheet);

		this.div.setAttribute('id','loading-spinner');
		this.div.style.cssText = "position: absolute;" +
			"top:45%;" +
			"left:45%;" +
			"background: url(../../images/logo.png);" +
			"background-size: cover;" +
			"box-shadow: 0px 0px 6px black;" +
			"border: 16px solid #f3f3f3;" +
			"border-radius: 50%;" +
			"border-top: 16px solid #3498db;" +
			"border-bottom: 16px solid #3d4c68;" +
			"width: 120px;" +
			"height: 120px;" +
			"-webkit-animation: spin 2s linear infinite;" +
			"animation: spin 2s linear infinite;";


			this.hide();
			this.parent.appendChild(this.div);
	}
	
	show(){
		this.div.style.display = 'flex';
	}
	
	hide(){
		this.div.style.display = 'none';
	}
}




