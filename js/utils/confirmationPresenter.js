import Presenter from "./core/presenter.js";

export default class ConfirmationPresenter extends Presenter{
    constructor(){
        super();
    }

    fillOrderId(){
        let orderID = new URL(window.location.toString()).searchParams.get('orderNumber');

        if(orderID != undefined && orderID != null){
            this.deleteCart();
        }

        document.querySelector('#orderId').textContent = orderID;
    }

    deleteCart(){
        localStorage.clear();
    }
}

