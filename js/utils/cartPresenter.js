import Presenter from './core/presenter.js';
import Spinner from './loading-spinner.js';

export default class CartPresenter extends Presenter{
    constructor(){
        super();
        this.productsCart = [];
        this.firstNameIsOk = false;
        this.lastNameIsOk = false;
        this.addressIsOk = false;
        this.cityIsOk = false;
        this.emailIsOk = false;
        this.cartItems = document.querySelector('#cart__items');
        this.spinner = new Spinner(this.cartItems);
    }

    //function which fill the cart.html page
    async fillCart(){
        let nbOfProducts = this.cart.length;
        let orderButton = document.querySelector('#order');

        //control if cart is empty
        if(nbOfProducts == 0){
            alert("Votre panier est vide");
            orderButton.disabled = true;
            return;
        }
        else{
            orderButton.disabled = false;
        }

        this.spinner.show();

        //creation of the products which have added to cart
        if(nbOfProducts == 0) return;

        let count = 0;

        for(let key in this.cart){
            let productCart = await this.getProductCart(key);
            this.productsCart.push(productCart);

            //fill the DOM elements with event listener for update the cart
            let totalQuantity = document.querySelector('#totalQuantity');
            let totalPrice = document.querySelector('#totalPrice');

            let aProductPage = document.createElement('a');
            aProductPage.setAttribute('href','./product.html?id=' + productCart.originalProduct._id);

            let article = document.createElement('article');
            article.classList.add("cart__item");
            article.setAttribute('id',productCart.originalProduct._id);
            article.setAttribute('data-color',productCart.color);

            let div = document.createElement('div');
            div.classList.add("cart__item__img");
            article.appendChild(div);

            let img = document.createElement('img');
            img.setAttribute('src',productCart.originalProduct.imageUrl);
            img.setAttribute('alt',productCart.originalProduct.altTxt);
            aProductPage.appendChild(img);
            div.appendChild(aProductPage);

            let div2 = document.createElement('div');
            div2.classList.add("cart__item__content");
            article.appendChild(div2);

            let div3 = document.createElement('div');
            div3.classList.add("cart__item__content__description");
            article.appendChild(div3);

            let h2 = document.createElement('h2');
            h2.textContent = productCart.originalProduct.name;
            div3.appendChild(h2);

            let p = document.createElement('p');
            p.textContent = productCart.color;
            div3.appendChild(p);

            let pPrice = document.createElement('p');
            pPrice.textContent = productCart.originalProduct.price + " €";
            div3.appendChild(pPrice);

            let div4 = document.createElement('div');
            div4.classList.add("cart__item__content__settings");
            article.appendChild(div4);

            let div5 = document.createElement('div');
            div5.classList.add("cart__item__content__settings__quantity");
            div4.appendChild(div5);

            let pQte = document.createElement('p');
            pQte.textContent = "Qte : ";
            div5.appendChild(pQte);

            let inputQuantity = document.createElement('input');
            inputQuantity.setAttribute('type','number');
            inputQuantity.setAttribute('name','itemQuantity');
            inputQuantity.setAttribute('min','1');
            inputQuantity.setAttribute('max','100');
            inputQuantity.classList.add('itemQuantity')
            inputQuantity.value = productCart.quantity;
            inputQuantity.addEventListener('input',(e)=>{
                e.preventDefault();
                
                this.setQuantityOfProductsInCart(productCart,e.target.value);
                this.setTotalPriceOfProductsInCart(productCart);
            });
            div5.appendChild(inputQuantity);

            let div6 = document.createElement('div');
            div6.classList.add("cart__item__content__settings__delete");
            article.appendChild(div6);

            let a = document.createElement('a');
            //a.textContent = "Supprimer";
            //a.setAttribute("data-idStorage",productCart.originalProduct._id + "|" + productCart.color);
            a.setAttribute("href","#");
            a.classList.add("deleteItem");
            a.addEventListener('click',(e)=>{
                e.preventDefault();
                this.deleteProductOfCart(e.target);
            });

            let iconeDelete = document.createElement('img');
            iconeDelete.setAttribute('src','../../images/icons/trash.svg');
            iconeDelete.setAttribute('alt','Supprimer la ligne de produit du panier');
            iconeDelete.setAttribute("data-idStorage",productCart.originalProduct._id + "|" + productCart.color);
            iconeDelete.style.height = '40px';
            iconeDelete.style.width = '80px';
            iconeDelete.style.textAlign = 'center';
            a.appendChild(iconeDelete);
            div6.appendChild(a);

            document.querySelector('#cart__items').appendChild(article);

            //if we are to the end of products list we update displayed infos in page bottom
            count++;
            if(count == nbOfProducts) {
                totalQuantity.textContent = this.getTotalQuantityOfProductsInCart();
                totalPrice.textContent = await this.getTotalPriceOfProductCart();

                setTimeout(()=>{

                    this.spinner.hide();
                    
                    },1000);

                //initialisation of formulary the sent datas
                this.initControlForm();

                //initialisation of button of datas to send
                orderButton.addEventListener('click',async (e)=>{
                    e.preventDefault();
                    if(this.isFormOk()){

                        let firstNameInput = document.querySelector('#firstName');
                        let lastNameInput = document.querySelector('#lastName');
                        let addressInput = document.querySelector('#address');
                        let cityInput = document.querySelector('#city');
                        let emailInput = document.querySelector('#email');

                        let contact = this.getNewContact(firstNameInput.value,lastNameInput.value,addressInput.value,cityInput.value,emailInput.value);

                        let orderNumber = await this.sendOrderToAPI(contact);
                        
                        if(orderNumber != null || orderNumber != undefined){
                            window.location = '../../html/confirmation.html?orderNumber=' + orderNumber.orderId;
                        }
                        else{
                            alert("Le serveur est temporairement inaccessible. Veuillez réessayer dans un instant.");
                        }
                    }
                    else{
                        alert("Votre formulaire de contact n'est pas complet ou des champs sont incorrects");
                    }
                });

                return;
            }
        }
    }

    //remove the product in cart
    deleteProductOfCart(target){
        try{
            //retrieve id and color from target (a)
            let idOfThisCart = target.getAttribute('data-idStorage');

            //retrieve productCart
            let productCart = this.productsCart.find((pdtCart)=>pdtCart.keyInThisCart == idOfThisCart);

            //retrieve article DOM
            let articleToDelete = document.querySelector('article[id="' + productCart.originalProduct._id + '"][data-color=\"' + productCart.color + '\"]');

            //update datas (quantity and total price)
            this.setQuantityOfProductsInCart(productCart,0)
            this.setTotalPriceOfProductsInCart(productCart,productCart.totalPrice);
            
            //delete productCart from cartPresenter array
            let indexOfProduct = this.productsCart.indexOf(productCart);
            this.productsCart.splice(indexOfProduct,indexOfProduct);

            //remove temporary productCart from this.cart array AT THE END
            this.cart.removeItem(productCart.keyInThisCart);

            document.querySelector('#cart__items').removeChild(articleToDelete);

            alert('Votre ' + productCart.originalProduct.name + ' ' + productCart.color + " a été supprimé du panier !");
        }
        catch(err){
            console.log('Erreur dans la fonction de suppression : ' + err);
        }
    }

    //upgrade the quantity of product in cart
    setQuantityOfProductsInCart(productCart,newValue){
        let totalQuantity = document.getElementById('totalQuantity');
        let diffQuantity = this.getDiff(productCart.quantity,newValue);

        productCart.quantity = parseInt(productCart.quantity) + diffQuantity;
        this.cart.setItem(productCart.keyInThisCart,productCart.quantity);
        totalQuantity.textContent = parseInt(totalQuantity.textContent) + diffQuantity; 
    }

    //upgrade the quantity of product in cart
    setTotalPriceOfProductsInCart(productCart){
        let totalPrice = document.getElementById('totalPrice');
        let newTotalPrice = productCart.originalProduct.price * productCart.quantity;
        let diffPrice = this.getDiff(productCart.totalPrice,newTotalPrice);
        
        productCart.totalPrice = newTotalPrice;
        totalPrice.textContent = parseInt(totalPrice.textContent) + diffPrice; 
    }

    //controls for form before send to order API
    initControlForm(){
        //control firstName
        let firstNameInput = document.querySelector('#firstName');

        firstNameInput.addEventListener('input',(e)=>{
            let firstNameMesgError = document.querySelector('#firstNameErrorMsg');
            let regexp = /^([a-zA-Z]){2,}([-|\s]{0,1}[a-zA-Z]{2,}){0,1}$/;
            let errorMessage = "* Le prénom doit contenir des lettres, seul les caractère (-, un espace) sont acceptés pour les prénoms composés";
            firstNameMesgError.textContent = this.controlExpression(e.target.value,regexp,errorMessage);

            this.firstNameIsOk = firstNameMesgError.textContent == "";
        });
        
        //control lastName
        let lastNameInput = document.querySelector('#lastName');

        lastNameInput.addEventListener('input',(e)=>{
            let lastNameMesgError = document.querySelector('#lastNameErrorMsg');
            let regexp = /^([a-zA-Z]){2,}([-|\s]{0,1}[a-zA-Z]{2,}){0,1}$/;
            let errorMessage = "* Le nom doit contenir des lettres, seul les caractère (-, un espace) sont acceptés pour les noms composés";
            lastNameMesgError.textContent = this.controlExpression(e.target.value,regexp,errorMessage);

            this.lastNameIsOk = lastNameMesgError.textContent == "";
        });

        //control address
        let addressInput = document.querySelector('#address');

        addressInput.addEventListener('input',(e)=>{
            let addressMesgError = document.querySelector('#addressErrorMsg');
            let regexp = /^([a-zéèêàA-Z]{2,}|[0-9]{1,})(\s{0,1}([a-zéèêàA-Z]-{0,1}){2,}){1,}/;
            let errorMessage = "* L'adresse doit commencer par un numéro de voie ou la voie directe s'il n'y a pas de numéro";
            addressMesgError.textContent = this.controlExpression(e.target.value,regexp,errorMessage);

            this.addressIsOk = addressMesgError.textContent == "";
        });

        //control city
        let cityInput = document.querySelector('#city');

        cityInput.addEventListener('input',(e)=>{
            let cityMesgError = document.querySelector('#cityErrorMsg');
            let regexp = /^([a-zA-Z]){2,}([-|\s]{0,1}[a-zA-Z]{2,}){0,}$/;
            let errorMessage = "* Le nom de ville doit contenir des lettres, seul les caractère (-, un espace) sont acceptés pour les noms de ville composés";
            cityMesgError.textContent = this.controlExpression(e.target.value,regexp,errorMessage);

            this.cityIsOk = cityMesgError.textContent == "";
        });

        //control email
        let emailInput = document.querySelector('#email');

        emailInput.addEventListener('input',(e)=>{
            let emailMesgError = document.querySelector('#emailErrorMsg');
            let regexp = /^([a-z0-9])([-|_|\.]{0,1}[a-z0-9]){0,}(@{1}[a-z]{2,}\.{1}[a-z]{2,})$/;
            let errorMessage = "* L'adresse email doit être de se format adresse.quelquechose@chose.fr, adresse47.quelques10choses@chose.fr";
            emailMesgError.textContent = this.controlExpression(e.target.value,regexp,errorMessage);

            this.emailIsOk = emailMesgError.textContent == "";
        });
    }

    isFormOk(){
        let formIsOk = (this.firstNameIsOk && this.lastNameIsOk && this.addressIsOk && this.cityIsOk && this.emailIsOk);
        
        return (formIsOk);
    }
}
