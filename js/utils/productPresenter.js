import Presenter from './core/presenter.js';

export default class ProductPresenter extends Presenter{
    constructor(){
        super();
    }

    //function which fill the DOM elements with a product in the product.html page
    async fillProduct(){
        let product = await this.getQueryProduct();

        //fill DOM elements
        document.title = product.name;

        let img = document.createElement('img');
        img.setAttribute('src',product.imageUrl);
        img.setAttribute('alt',product.altTxt);
        document.querySelector('div.item__img').appendChild(img);
        document.querySelector('#title').textContent = product.name;
        document.querySelector('#price').textContent = product.price;
        document.querySelector('#description').textContent = product.description;
        
        product.colors.forEach((color)=>{
            let option = document.createElement('option');
            option.setAttribute('value',color);
            option.textContent = color;
            document.querySelector('#colors').appendChild(option);
        });

        document.querySelector('#addToCart').addEventListener('click',(e)=>{
            this.addToCart();
        });
    }

    //add product to cart from the product.html page
    addToCart(){
        
        let quantityOfProduct = document.querySelector('#quantity').value;
        let selectedColor = document.querySelector('#colors').value;

        //check if color is selected
        if(selectedColor === "") {
            alert("Veuillez sélectionner une couleur s'il vous plait");
            return;
        }

        let key = this.getIdProduct() + "|" + selectedColor;

        //check if item exist
        if(this.cart.getItem(key)){
            let quantity = parseInt(this.cart.getItem(key)) + parseInt(quantityOfProduct);
            this.cart.setItem(key, quantity);
        }
        else{
            this.cart.setItem(key, quantityOfProduct);
        }
        
        alert("Vous avez ajouté " + quantityOfProduct + " " + document.querySelector('#title').textContent + " dans votre panier");
    }
}