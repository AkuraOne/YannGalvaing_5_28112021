import Presenter from './core/presenter.js';
import Spinner from './loading-spinner.js';

export default class indexPresenter extends Presenter{
    constructor(){
        super();
        this.items = document.getElementById('items');
        this.spinner = new Spinner(this.items);
    }

    //function which fill the DOM elements with all products in the index.html page
    async fillProducts(){
        this.spinner.show();

        //query for get all products
        let products = await this.getQueryAllProducts();

        //fill the DOM elements index Page
        products.forEach(product => {
            let a = document.createElement('a');
            a.setAttribute('href',"./product.html?id=" + product._id);

            let article = document.createElement('article');

            let image = document.createElement('img');
            image.setAttribute('src',product.imageUrl);
            image.setAttribute('alt',product.altTxt);

            let h3 = document.createElement('h3');
            h3.classList.add("productName");
            h3.textContent = product.name;

            let p = document.createElement('p');
            p.classList.add("productDescription");
            p.textContent = product.description;

            article.appendChild(image);
            article.appendChild(h3);
            article.appendChild(p);
            a.appendChild(article);

            this.items.appendChild(a);
        });

        setTimeout(()=>{

        this.spinner.hide();
		
        },1000);
        
  }
}