// kanap-index.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:

const expect = require("chai").expect;

// https://on.cypress.io/writing-first-test
describe('home page tests',()=>{

    before(()=>{
        cy.clearLocalStorageSnapshot();
    });

    beforeEach(()=>{
        cy.restoreLocalStorage();
    });

    afterEach(()=>{
        cy.saveLocalStorage();
    });

    it('home page structure',()=>{
        cy.visit('./index.html');

        cy.get('[href="./index.html"]');
        cy.get('[href="./cart.html"]');
    });

    after('after loading document',()=>{
        cy.get('[href="./product.html?id=107fb5b75607497b96722bda5b504926"]');
        cy.get('[href="./product.html?id=415b7cacb65d43b2b5c1ff70f3393ad1"]');
        cy.get('section#items').children('a').should('have.length',8);
    });
});

describe('product html page tests',()=>{

    beforeEach(()=>{
        cy.restoreLocalStorage();
    });

    afterEach(()=>{
        cy.saveLocalStorage();
    });

    it('surf to home page',()=>{
        cy.visit('./product.html?id=107fb5b75607497b96722bda5b504926');
    });

    it('test if selected color is null',()=>{
        cy.get('select#colors').select(0).should('have.value',"");
    });

    it('select values for add new product in cart', ()=>{
        cy.get('select#colors').select(1)/*.pause()*/;
        cy.get('input#quantity').type("{backspace}20")/*.pause()*/;
    });

    it('test color value selected in colors list',()=>{
        cy.get('select#colors option:selected').should('have.value','Blue')/*.pause()*/;
    });

    it("test quantity selected in input value",()=>{
        cy.get('input#quantity').invoke('val').then((val)=> expect(val).eq('20'));
    });

    it("test if button isn't disabled",()=>{
        cy.get('button#addToCart').should('not.be.disabled')/*.pause()*/;
    });

    it('add product to cart',()=>{
        
        cy.get('button#addToCart').click();
    });
    
    it('test value in localstorage',()=>{
        cy.getLocalStorage("107fb5b75607497b96722bda5b504926|Blue").then((val)=>expect(val).eq("20"))/*.pause()*/;
    });
});

describe('Select new product',()=>{

    beforeEach(()=>{
        cy.restoreLocalStorage();
    });

    afterEach(()=>{
        cy.saveLocalStorage();
    });

    it('surf on product page',()=>{
        cy.visit("./product.html?id=415b7cacb65d43b2b5c1ff70f3393ad1");
    });

    it('Select values IHM for add the product',()=>{
        cy.get('select#colors').select(1);
        cy.get('input#quantity').type('{backspace}10');
    });

    it('test selected color in selected value',()=>{
        cy.get('select#colors option:selected').should('have.value','Black/Yellow')/*.pause()*/;
    });

    it('test quantity in input value',()=>{
        cy.get('input#quantity').invoke('val').then((val)=> expect(val).eq('10'));
    });

    it('add second product in cart',()=>{
        cy.get('button#addToCart').click();
    });

    it('test value in localstorage',()=>{
        cy.getLocalStorage("415b7cacb65d43b2b5c1ff70f3393ad1|Black/Yellow").then((val)=> expect(val).eq("10"));
    });
});

describe('cart html page tests',()=>{

    beforeEach(()=>{
        cy.restoreLocalStorage();
    });

    afterEach(()=>{
        cy.saveLocalStorage();
    });

    after(()=>{
        cy.clearLocalStorageSnapshot();
    });


    it('surf on cart page',()=>{
        cy.visit('./cart.html');
    });

    it('test localstorage values',()=>{
        cy.getLocalStorage("107fb5b75607497b96722bda5b504926|Blue").then((val)=>expect(val).eq("20"))/*.pause()*/;
        cy.getLocalStorage("415b7cacb65d43b2b5c1ff70f3393ad1|Black/Yellow").then((val)=> expect(val).eq("10"));
    });

    it('test products number in cart', () => {
        cy.get('section#cart__items').children('article').should('have.length',2)/*.pause()*/;
    });

    it("test order button status isn't disabled",()=>{
        cy.get('input#order').should('not.be.disabled')/*.pause()*/;
    });

    it('product page fill the contact form', () => {
        cy.get('input#firstName').type('Luc',{ force: true, release: true });
        cy.get('input#lastName').type('Skywalker',{ force: true, release: true });
        cy.get('input#address').type('1024 route de l\'octet',{ force: true, release: true });
        cy.get('input#city').type('Mos Espa',{ force: true, release: true });
        cy.get('input#email').type('luc.skywalker@tatooine.com',{ force: true, release: true })/*.pause()*/;
    });

    it('order validation',()=>{
        cy.get('input#order').click();
    });
});